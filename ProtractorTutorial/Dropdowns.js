describe('Chain locators demo',function() {  

	function calc (a,b,c) {
	    element(by.model("first")).sendKeys(a);
	    element(by.model("second")).sendKeys(b);
	    
		element.all(by.tagName('option')).each( function (item)
			{
			
			item.getAttribute("value").then(function(values)
					
					{
							
				if(values==c)
					{
					item.click();
					}
			})
			
		})
		
element(by.id('gobutton')).click();	
	
	}

it('Open Angular js website',function() {

     
    browser.get('http://juliemr.github.io/protractor-demo/')
    
    calc(2,3,"DIVISION");
    calc(1,3,"DIVISION");
    calc(211,3,"DIVISION");
    calc(2,3,"DIVISION");


	element.all(by.repeater('result in memory')).each(function(item) 
			{
		
			item.element(by.css('td:nth-child(3)')).getText().then( function (text)
				{
				console.log(text);
			
				})
			})
	})

})
    	
    	
    	
	